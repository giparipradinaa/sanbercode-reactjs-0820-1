// soal satu

console.log("LOOPING PERTAMA");
var baris = 2;
while( baris <= 20 ) {
    console.log( baris + " - I love coding");
    baris += 2;
}

console.log("LOOPING KEDUA");
var baris = 20;
while( baris >= 2 ) {
    console.log( baris + " - I will become a frontend developer");
    baris -=2;
}

// soal dua

for( var baris = 1; baris <= 20; baris++ ) {
    if( baris % 3 == 0 && baris % 2 == 1 ) {
        console.log( baris + " - I Love Coding");
    } else if( baris % 2 == 1  ) {
        console.log( baris + " - Santai");
    } else if ( baris % 2 == 0 ) {
        console.log( baris + " - Berkualitas");
    } 
}

// soal tiga

var s = '';
for ( var i = 0; i < 7; i++ ) {
    for ( var j = 0; j <= i; j++ ) {
        s += '#';
    }
    s += '\n';
}

console.log(s);

// soal empat

var kalimat = "saya sangat senang belajar javascript"
var kalimatArray = kalimat.split(" ");
console.log(kalimatArray);

// soal lima

var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
var urutanDaftarBuah = daftarBuah.sort()

for( nomor = 0; nomor < 5; nomor++ ) {
    console.log(urutanDaftarBuah[nomor]);
}
// soal satu

var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

console.log(kataPertama + " " + kataKedua.charAt(0).toUpperCase() + kataKedua.substring(1) + " " + kataKetiga + " " + kataKeempat.toUpperCase()); // jawaban soal satu



// soal dua

var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "5";

var intKataPertama = parseInt(kataPertama);
var intKataKedua = parseInt(kataKedua);
var intKataKetiga = parseInt(kataKetiga);
var intKataKeempat = parseInt(kataKeempat);

console.log(intKataPertama + intKataKedua + intKataKetiga + intKataKeempat); // jawaban soal dua



// soal tiga

var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14); // jawaban soal tiga
var kataKetiga = kalimat.substring(15, 18); // jawaban soal tiga
var kataKeempat = kalimat.substring(19, 24); // jawaban soal tiga
var kataKelima = kalimat.substring(25); // jawaban soal tiga

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);



// soal empat

var nilai;
nilai = 75;

if( nilai >= 80) {
    console.log("A");
} else if( nilai >= 70 && nilai < 80 ) {
    console.log("B");
} else if( nilai >= 60 && nilai < 70 ) {
    console.log("c");
} else if( nilai >= 50 && nilai < 60 ) {
    console.log("D");
} else if( nilai < 50 ) {
    console.log("E");
}



// soal lima

var tanggal = 22;
var bulan = 7;
var tahun = 2020;

tanggal = 5;
bulan = 4;
tahun = 1999;

switch(bulan) {
    case 1: { bulan = "Januari"; break; }
    case 2: { bulan = "Februari"; break; }
    case 3: { bulan = "Maret"; break; }
    case 4: { bulan = "April"; break; }
    case 5: { bulan = "Mei"; break; }
    case 6: { bulan = "Juni"; break; }
    case 7: { bulan = "Juli"; break; }
    case 8: { bulan = "Agustus"; break; }
    case 9: { bulan = "September"; break; }
    case 10: { bulan = "Oktober"; break; }
    case 11: { bulan = "November"; break; }
    case 12: { bulan = "Desember"; break; }
}

console.log(tanggal +" "+ bulan +" "+ tahun);
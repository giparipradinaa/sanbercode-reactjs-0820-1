// soal satu

const luasLingkaran = (jariJari) => {
    const phi = 3.14;
    return phi * jariJari * jariJari;
}

const kelilingLingkaran = (jariJari) => {
    let phi = 3.14;
    return  2 * phi * jariJari;
}

console.log("Luas lingkarang adalah = " + luasLingkaran(10));
console.log("Luas lingkarang adalah = " + kelilingLingkaran(10));


// soal dua

let kalimat = "";

const tambahKata = (...rest) => {
    kalimat = `${rest[0]} ${rest[1]} ${rest[2]} ${rest[3]} ${rest[4]}`;
}

tambahKata("saya", "adalah", "seorang", "frontend", "developer");

console.log(kalimat);



// soal tiga

const newFunction = (firstName, lastName) => {
    return {
        firstName: firstName,
        lastName: lastName,
        fullName: () => {
            console.log(firstName + " " + lastName)
        return 
      }
    }
}

newFunction("William", "Imoh").fullName();


// soal empat

const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

let {firstName, lastName, destination, occupation, spell} = newObject;

console.log(firstName, lastName, destination, occupation);


// soal lima

const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];

let combined = [...west, ...east];
console.log(combined);
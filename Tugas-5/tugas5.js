// soal 1

function halo() {
    return "Hallo Sanbers!";
}
 
console.log(halo());


// soal dua

function kalikan( angka1, angka2 ) {
    return angka1 * angka2;
}
 
var num1 = 12;
var num2 = 4;
 
var hasilKali = kalikan(num1, num2);
console.log(hasilKali);


// soal tiga

function introduce(nama, umur, alamat, hobi) {
    var kalimat = "Nama saya " + nama + " umur saya " + umur + " tahun, alamat saya di " + alamat + ", dan saya punya hobby yaitu " + hobi + "!";
    return kalimat;
}
 
var name = "John";
var age = 30;
var address = "Jalan belum jadi";
var hobby = "Gaming";
 
var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan);


// soal empat

var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku" , 1992]

var objDaftarPeserta = [];
objDaftarPeserta.nama = arrayDaftarPeserta[0];
objDaftarPeserta["jenis kelamin"] = arrayDaftarPeserta[1];
objDaftarPeserta.hobi = arrayDaftarPeserta[2];
objDaftarPeserta["tahun lahir"] = arrayDaftarPeserta[3];

console.log(objDaftarPeserta);


// soal lima

var arrBuah = [];

function tambahDataBuah( dataNama, dataWarna, dataAdaBijinya, dataHarga ) {
    var item = { nama: dataNama, warna: dataWarna, "ada bijinya": dataAdaBijinya, harga: dataHarga };
    return item;
}

var satu = tambahDataBuah("strawberry", "merah", "tidak", "9000");
var dua = tambahDataBuah("jeruk", "oranye", "ada", "8000");
var tiga = tambahDataBuah("Semangka", "Hijau & Merah", "ada", "10000");
var empat = tambahDataBuah("Pisang", "Kuning", "tidak", "5000");

arrBuah.push(satu);
arrBuah.push(dua);
arrBuah.push(tiga);
arrBuah.push(empat);

// console.log(arrBuah);

// console.log(arrBuah[0]);

function tampilData(e) {
    console.log("nama : " + e.nama );
    console.log("warna : " + e.warna );
    console.log("ada bijinya : " + e["ada bijinya"]);
    console.log("harga : " + e.harga );
}

tampilData(arrBuah[0]);


//  soal enam

var dataFilm = [];

function tambahDataFilm( dataNama, dataDurasi, dataGenre, dataTahun ) {
    var item = { nama: dataNama, durasi: dataDurasi, genre: dataGenre, tahun: dataTahun };
    return dataFilm.push(item);
}

tambahDataFilm("Avenger", "3 jam", "Action", 2019);
tambahDataFilm("Spiderman", "2 jam", "Action", 2019);

console.log(dataFilm);
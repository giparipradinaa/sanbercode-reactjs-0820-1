// soal satu
    // release 0

class Animal {
    constructor(name) {
        this.name = name;
        this._legs = 4;
        this._cold_blooded = false;
    }
}

var sheep = new Animal("shaun");

console.log(sheep.name);
console.log(sheep._legs);
console.log(sheep._cold_blooded);

console.log("\n");

    // release 1

class Frog extends Animal {
    constructor(name) {
        super(name);
        this._legs;
        this._cold_blooded;
    }

    jump() {
        return console.log("hop hop");
    }
}

class Ape extends Animal {
    constructor(name) {
        super(name);
        this._legs;
        this._cold_blooded;
    }

    get legs() {
        return this._legs;
    }

    set legs(x) {
        this._legs = x;
    }

    yell() {
        return console.log("Auooo");
    }
}

var sungokong = new Ape("Kera Sakti");
sungokong.yell();

console.log("\n");

var kodok = new Frog("buduk");
kodok.jump();

console.log("\n");

// soal dua

class Clock {
    constructor({ template }) {
        this._template = template;
        this.timer;
    }

    render() {

        let date = new Date();
        
        let hours = date.getHours();
        if (hours < 10) hours = '0' + hours;
        
        let mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;
        
        let secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;
        
        let output = this._template
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs);
    
        console.log(output);
        }
  
    stop() {
      clearInterval(this.timer);
    };
  
    start() {
      this.render.bind(this);
      this.timer = setInterval(this.render.bind(this), 1000);
    };
  
}

var clock = new Clock({template: 'h:m:s'});
clock.start(); 